//
//  AppDelegate.swift
//  AClock
//
//  Created by Mint Rocket on 05.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions
        launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.applicationIconBadgeNumber = 0
        let window = UIWindow(frame: UIScreen.main.bounds)
        let dependencyConfiguration = DependenciesConfigurationBase()
        let coordinator = MainAppCoordinator(configuration: dependencyConfiguration)
        coordinator.window = window
        MainAppCoordinator.shared = coordinator
        coordinator.start()
        return true
    }
}

