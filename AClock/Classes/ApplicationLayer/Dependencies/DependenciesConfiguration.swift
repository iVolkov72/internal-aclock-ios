//
//  DependenciesConfiguration.swift
//  AClock
//
//  Created by Mint Rocket on 05.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import Foundation

import UIKit
import Swinject
import Localize_Swift
import IQKeyboardManagerSwift

public protocol DependenciesConfiguration: class {
    func setup()
    func setupModulesDependencies()
    func setupSharedDependencies()
    func configuredContainer() -> Container
}

public class DependenciesConfigurationBase: DependenciesConfiguration, Loggable {
    
    // MARK: - Configure
    public var defaultLoggingTag: LogTag {
        return .Unnamed
    }
    
    public func configuredContainer() -> Container {
        return Container(registeringClosure: { container in
            self.registerStores(container: container)
            self.registerServices(container: container)
            self.registerSingletonServices(container: container)
        })
    }
    
    private func registerStores(container: Container) {
    }
    
    private func registerServices(container: Container) {
    }
    
    private func registerSingletonServices(container: Container) {
    }
    
    // MARK: - Setup
    
    public func setup() {
        self.setupModulesDependencies()
        self.setupSharedDependencies()
        self.setupLoader()
        self.setupKeyboardController()
    }
    
    public func setupModulesDependencies() {
        // configuration
        //  Configuration.shared = Configuration()
        //   MainTheme.shared = MainTheme()
        
        // backend service
        //   let backendURL = URL(string: Configuration.shared.apiUrl())!
        //   BackendConfiguration.shared = BackendConfiguration(baseURL: backendURL,
        //                                                      converter: JsonResponseConverter())
        
        // logger
        let logger = Logger()
        let swiftyLogger = SwiftyBeaverLogger()
        logger.setupLogger(swiftyLogger)
        Logger.setSharedInstance(logger)
        
        // error handling
        let errorHandler = ErrorHandler()
        let alertHandler = AlertErrorHandler()
        errorHandler.setupHandler(alertHandler)
        ErrorHandler.setSharedInstance(errorHandler)
    }
    
    public func setupSharedDependencies() {
        //Fabric.with([Crashlytics.self])
    }
    
    func setupLoader() {
        //MRViewContainer.configure(with: MRLoaderView.self)
    }
    
    func setupKeyboardController() {
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldPlayInputClicks = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().toolbarManageBehaviour = .byPosition
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    }
}


