//
//  Test.swift
//  AClock
//
//  Created by Mint Rocket on 05.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import Foundation
import UIKit
import Swinject

public class MainAppCoordinator: Loggable {
    
    public var defaultLoggingTag: LogTag {
        return .Service
    }
    
    open var window: UIWindow!
    open static var shared: MainAppCoordinator!
    open var configuration: DependenciesConfiguration!
    open var container: Container
    //open var settings: Settings
    
    init(configuration: DependenciesConfiguration) {
        self.configuration = configuration
        self.configuration.setup()
        self.container = self.configuration.configuredContainer()
        //self.settings = Settings()
        self.log(.debug, "Dependencies are configured")
    }
    
    func start() {
        //  MainTheme.shared.apply()
        self.openMainModule()
        self.log(.debug, "App coordinator started")
        self.registerNotification()
    }
    
    func registerNotification() {
        let registration = UIUserNotificationSettings.init(types: [.sound, .alert], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(registration)
    }
    
    open func openMainModule() {
        let mainContainerModule = MainContainerAssembly.createModule()
        mainContainerModule.router.present(mainContainerModule, using: window)
    }    
}
