//
//  AClockItem.swift
//  AClock
//
//  Created by Mint Rocket on 06.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import UIKit
import Foundation
import UserNotifications

public class AClockItem: NSObject, Encodable, Decodable {
    var key: Int = 0
    var switchState: Bool = false
    var date: Date?
    
    convenience init(notification: UILocalNotification) {
        self.init()
        self.date = notification.fireDate
        if let key = notification.userInfo?["key"] as? Int {
            self.key = key
            self.switchState = true
        }
    }
    
    /*func creatAClockItem(notification: UILocalNotification) -> AClockItem {
        let clockItem: AClockItem = AClockItem()
        clockItem.date = notification.fireDate
        
        if let key = notification.userInfo?["key"] as? Int {
            clockItem.key = key
            clockItem.switchState = true
        }
        return clockItem
    }*/
}
