//
//  MRAddClockAssembly.swift
//  AClock
//
//  Created by Puls7289 on 05/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

//var action: ((String, Int)-> String)?

final class AddClockAssembly {
    class func createModule(completion: ((Date?) -> ())?) -> AddClockViewController {
        let vc = StoryboardScene.Main.addClockViewController.instantiate()
        let presenter = AddClockPresenter(completion: completion)
        let router = AddClockRouter()
        
//        action?()
//        action = { context in
//            print(context)
//            return ""
//        }
        presenter.view = vc
        vc.handler = presenter
        vc.router = router

        router.viewController = vc

        return vc
    }
}
