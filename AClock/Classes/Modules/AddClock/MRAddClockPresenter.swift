//
//  MRAddClockPresenter.swift
//  AClock
//
//  Created by Puls7289 on 05/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Presenter
final class AddClockPresenter {

    weak var view: AddClockViewBehavior!
    private var handler: ((Date?) -> ())?
    private var date: Date?
    init(completion: ((Date?) -> ())?) {
        self.handler = completion
    }
    
    deinit {
        handler?(date)
    }
}

extension AddClockPresenter: AddClockEventHandler {

    func put(date: Date?) {
        self.date = date
    }
    
    func getTime() {
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = DateFormatter.Style.medium
        view.setTime(time: timeFormatter)
    }
}
