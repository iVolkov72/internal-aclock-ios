//
//  MRAddClockRouter.swift
//  AClock
//
//  Created by Puls7289 on 05/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Router
final class AddClockRouter: VIPERRouter<AddClockViewController> {
    func returnToClockList() {
        let vc = ListClockAssembly.createModule()
        if let navigation = self.viewController.navigationController {
            self.present(vc, using: navigation)
        }
    }
}
