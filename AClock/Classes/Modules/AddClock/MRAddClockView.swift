//
//  MRAddClockkView.swift
//  AClock
//
//  Created by Puls7289 on 05/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - View Controller
final class AddClockViewController: BaseViewController {
    
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var newTimeLabel: UILabel!
    
    var handler: AddClockEventHandler!
    typealias RouterType = AddClockRouter
    var router: RouterType!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        timePicker.setValue(UIColor.white, forKey: "textColor")
    }
    
    @IBAction func didTapAddTimeButton(_ sender: Any) {
        handler.getTime()
        listClock()
    }
    
    func listClock() {
        handler.put(date: timePicker.date)
        router.dismiss()
    }
}

extension AddClockViewController: AddClockViewBehavior {
    func setTime(time: DateFormatter) {
        timePicker.timeZone = NSTimeZone.default
        newTimeLabel.text = time.string(from: timePicker.date)
    }
}
