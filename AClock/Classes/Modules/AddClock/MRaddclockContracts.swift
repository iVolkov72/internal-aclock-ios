//
//  MRAddClockContracts.swift
//  AClock
//
//  Created by Puls7289 on 05/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Contracts
protocol AddClockViewBehavior: class {
    func setTime(time: DateFormatter)
}

protocol AddClockEventHandler: class {
    func put(date: Date?)
    func getTime()
}
