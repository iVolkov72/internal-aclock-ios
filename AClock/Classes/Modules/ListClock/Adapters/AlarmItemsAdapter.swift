//
//  AlarmItemsAdapter.swift
//  AClock
//
//  Created by Mint Rocket on 09.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import Foundation
import IGListKit

protocol AlarmItemsAdapterDelegate: class {
    func switchPressedOn(item: AClockItem)
}

public class AlarmItemsAdapter: ListSectionController, AlarmItemCellDelegate {

    func switchOn(item: AClockItem) {
        self.delegate?.switchPressedOn(item: item)
    }
    
    private var object: AClockItem?
    weak var delegate: AlarmItemsAdapterDelegate?
    
    override public func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 80)
    }
    
    public override func cellForItem(at index: Int) -> UICollectionViewCell {
        let alarmItemCell = collectionContext!.dequeueReusableCell(withNibName: AlarmItemCell.className(),
                                                                      bundle: nil,
                                                                      for: self,
                                                                      at: index) as? AlarmItemCell
        if let object = self.object {
            alarmItemCell?.configure(data: object)
            alarmItemCell?.aitDelegate = self
            alarmItemCell?.backgroundView?.backgroundColor = UIColor.blue
        }
        return alarmItemCell!
    }
    
    override public func didUpdate(to object: Any) {
        self.object = object as? AClockItem
    }
}

extension NSObject: ListDiffable {
    public func diffIdentifier() -> NSObjectProtocol {
        return self
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return isEqual(object)
    }
}

extension UICollectionViewCell {
    static func className() -> String {
        return String(describing: self)
    }
}
