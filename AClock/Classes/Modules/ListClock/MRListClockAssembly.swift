//
//  MRListClockAssembly.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

final class ListClockAssembly {
    class func createModule() -> ListClockViewController {
        let vc = StoryboardScene.Main.listClockViewController.instantiate()
        let presenter = ListClockPresenter()
        let router = ListClockRouter()

        presenter.view = vc
        vc.handler = presenter
        vc.router = router

        router.viewController = vc

        return vc
    }
}
