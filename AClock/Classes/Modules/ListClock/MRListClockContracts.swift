//
//  MRListClockContracts.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Contracts
protocol ListClockViewBehavior: class {
    func set(item: AClockItem)
    func set(items: [AClockItem])
}

protocol ListClockEventHandler: class {
    func createClockItem(date: Date)
    func updateClockItem(item: AClockItem)
    func restorePushes() 
}

