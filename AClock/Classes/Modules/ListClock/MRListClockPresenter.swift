//
//  MRListClockPresenter.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit
import DefaultsKit

// MARK: - Presenter
final class ListClockPresenter {

    weak var view: ListClockViewBehavior!
    let push = PushService()
}

extension ListClockPresenter: ListClockEventHandler {
    func createClockItem(date: Date) {
        let notification = push.createNotification(time: date)
        let item = push.createItem(notification: notification)
        view.set(item: item)
    }
    
    func updateClockItem(item: AClockItem) {
        if item.switchState {
            item.switchState = false
            push.deletePush(item: item)
        } else {
            item.switchState = true
            
            if let date = item.date {
                push.createNotification(time: date)
            }
        }
        push.updateItem(item: item)
    }
    
    func restorePushes() {
        if let items = push.restoreAllPushes() {
            view.set(items: items)
        }
    }
}
