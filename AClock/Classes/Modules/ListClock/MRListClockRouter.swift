//
//  MRListClockRouter.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Router
final class ListClockRouter: VIPERRouter<ListClockViewController> {
    
    func openAddClock(completion: ((Date?) -> ())?) {
        let vc = AddClockAssembly.createModule(completion: completion)
        if let navigation = self.viewController.navigationController {
            self.present(vc, using: navigation)
        }
    }
}


