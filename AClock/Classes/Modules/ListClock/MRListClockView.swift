//
//  MRMRListClockView.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit
import IGListKit



// MARK: - View Controller
final class ListClockViewController: BaseCollectionViewController, AlarmItemsAdapterDelegate {
    func switchPressedOn(item: AClockItem) {
        handler.updateClockItem(item: item)
    }

    @IBOutlet weak var clockCollectionView: UICollectionView!
    
    var handler: ListClockEventHandler!
    typealias RouterType = ListClockRouter
    var router: RouterType!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBar()
        clockCollectionView.backgroundColor = UIColor.clear
        handler.restorePushes()
        
        /*
        let pushes = LocalPush()
        for push in pushes.getActiveNotifications() {
            if let date = push.value.fireDate {
                self.handler.checkSwitchState(date: date, state: true)
            }
        }
        */
    }
    
    func addNavBar() {
        let addBarButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addClock))
        navigationItem.setRightBarButton(addBarButton, animated: true)
        navigationController?.navigationBar.barTintColor = nil
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        navigationController?.navigationBar.backgroundColor = UIColor.clear.withAlphaComponent(0.0)
    }
    
    @objc func addClock() {
        router.openAddClock { [weak self] date in
            if let date = date {
                self?.handler.createClockItem(date: date)
            }
        }
    }
    
    override func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let adapter = AlarmItemsAdapter()
        adapter.delegate = self
        return adapter
    }
}

extension ListClockViewController: ListClockViewBehavior {
    func set(items: [AClockItem]) {
        self.items = items
        self.update()
    }
    
    func set(item: AClockItem) {
        self.items.append(item)
        self.update()
    }
}
