//
//  AlarmItemCell.swift
//  AClock
//
//  Created by Mint Rocket on 09.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import Foundation
import UIKit

protocol AlarmItemCellDelegate: class {
    func switchOn(item: AClockItem)
}
class AlarmItemCell: UICollectionViewCell {

    var notificationDate: Date?
    var item: AClockItem?
    weak var aitDelegate: AlarmItemCellDelegate?
    
    //var classObj : ListClockViewController!
    @IBOutlet weak var alarmTimeLabel: UILabel!
    @IBOutlet weak var alarmSwitch: UISwitch!

    func configure(data: AClockItem) {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        self.alarmTimeLabel.text = formatter.string(from: (data.date)!)
        self.alarmSwitch.isOn = data.switchState
        item = data
    }
    
    @IBAction func alarmChanged(_ sender: Any) {
        if let clockitem = item {
            self.aitDelegate?.switchOn(item: clockitem)
        }
    }
}
