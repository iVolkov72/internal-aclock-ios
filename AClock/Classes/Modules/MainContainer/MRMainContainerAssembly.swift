//
//  MRMainContainerAssembly.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

final class MainContainerAssembly {
    class func createModule() -> MainContainerViewController {
        let vc = StoryboardScene.Main.mainContainerViewController.instantiate()
        let presenter = MainContainerPresenter()
        let router = MainContainerRouter()

        presenter.view = vc
        vc.handler = presenter
        vc.router = router

        router.viewController = vc

        return vc
    }
}
