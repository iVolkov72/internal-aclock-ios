//
//  MRMainContainerContracts.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Contracts
protocol MainContainerViewBehavior: class {
    func setControllers(views: [UIViewController])
}

protocol MainContainerEventHandler: class {
    func getControllers()
}
