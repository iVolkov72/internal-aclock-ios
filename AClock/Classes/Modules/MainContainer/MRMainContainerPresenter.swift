//
//  MRMainContainerPresenter.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Presenter
final class MainContainerPresenter {

    weak var view: MainContainerViewBehavior!

}

extension MainContainerPresenter: MainContainerEventHandler {
    func getControllers() {
        let listVc = ListClockAssembly.createModule()
        let settingsVc = SettingsClockAssembly.createModule()
        let navigVc = BaseNavigationController.init(rootViewController: listVc)

        let imageSetTab = #imageLiteral(resourceName: "icons8-Automation-26")
        let imageListTab = #imageLiteral(resourceName: "Rectangle 54-1")
        
        let listTab = UITabBarItem.init(title: "Будильник", image: imageListTab, tag: 0)
        let settTab = UITabBarItem.init(title: "Настройки", image: imageSetTab, tag: 1)
        
        navigVc.tabBarItem = listTab
        settingsVc.tabBarItem = settTab
        view.setControllers(views: [navigVc, settingsVc])
    }
}
