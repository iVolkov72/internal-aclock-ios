//
//  MRMRMainContainerView.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - View Controller
final class MainContainerViewController: UITabBarController {
    
    var handler: MainContainerEventHandler!
    typealias RouterType = MainContainerRouter
    var router: RouterType!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        handler.getControllers()
    }
    
    func setView() {
        
    }

}

extension MainContainerViewController: MainContainerViewBehavior {
    func setControllers(views: [UIViewController]) {
        self.setViewControllers(views, animated: true)
    }
}
