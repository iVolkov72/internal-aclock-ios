//
//  MRSettingsClockAssembly.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

final class SettingsClockAssembly {
    class func createModule() -> SettingsClockViewController {
        let vc = StoryboardScene.Main.settingsClockViewController.instantiate()
        let presenter = SettingsClockPresenter()
        let router = SettingsClockRouter()

        presenter.view = vc
        vc.handler = presenter
        vc.router = router

        router.viewController = vc

        return vc
    }
}
