//
//  MRSettingsClockContracts.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Contracts
protocol SettingsClockViewBehavior: class {

}

protocol SettingsClockEventHandler: class {

}