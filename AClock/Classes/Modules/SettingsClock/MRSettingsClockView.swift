//
//  MRMRSettingsClockView.swift
//  AClock
//
//  Created by Puls7289 on 06/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - View Controller
final class SettingsClockViewController: BaseViewController {
    
    var handler: SettingsClockEventHandler!
    typealias RouterType = SettingsClockRouter
    var router: RouterType!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension SettingsClockViewController: SettingsClockViewBehavior {

}