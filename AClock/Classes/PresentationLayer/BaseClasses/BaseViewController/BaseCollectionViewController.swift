//
//  BaseCollectionViewController.swift
//  Inventory+
//
//  Created by Kirill Kunst on 03/05/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import Foundation
import IGListKit

class BaseCollectionViewController: BaseViewController, ListAdapterDataSource {

    // MARK: - Outlets
    @IBOutlet var collectionView: UICollectionView!
    var items: [ListDiffable] = []
    var emptyView: UIView?

//    var canLoad = false

    // MARK: - Properties
    //private var refreshControl: SpringIndicator.Refresher?

    public lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 3)
    }()

//    public var scrollViewDelegate: UIScrollViewDelegate? {
//        get {
//            return adapter.scrollViewDelegate
//        }
//        set {
//            adapter.scrollViewDelegate = newValue
//        }
//    }

    // MARK: - Setup
    override func setup() {
        super.setup()
        self.adapter.collectionView = self.collectionView
        self.adapter.dataSource = self
//        self.scrollViewDelegate = self
    }

    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.updateRefreshControlRect()
    }

    // MARK: - Refresh
//    public func addRefreshControl(color: UIColor = MainTheme.shared.blue) {
//        if self.refreshControl != nil {
//            return
//        }
//        self.collectionView.alwaysBounceVertical = true
//        self.refreshControl = SpringIndicator.Refresher()
//        self.refreshControl?.indicator.lineColor = color
//        self.collectionView.addSubview(self.refreshControl!)
//        self.refreshControl?.addTarget(self,
//                                       action: #selector(self.refresh),
//                                       for: .valueChanged)
//    }

    public func removeRefreshControl() {
        //self.refreshControl?.removeFromSuperview()
        //self.refreshControl = nil
    }

    public func updateRefreshControlRect() {
        //self.refreshControl?.center.x = (self.view.frame.width)/2
    }

    public func stopRefreshing() {
        //self.refreshControl?.endRefreshing()
    }

//    public func isRefreshing() -> Bool {
//        return self.refreshControl?.refreshing ?? false
//    }

    public func refresh() {
        //override me
    }

    public func loadMore() {
        //override me
    }

    // MARK: - Methods
    public func update(animated: Bool = true, completion: ListUpdaterCompletion? = nil) {
        adapter.performUpdates(animated: animated, completion: completion)
    }

    public func reload(completion: ListUpdaterCompletion? = nil) {
        adapter.reloadData(completion: completion)
    }

    // MARK: - IGListAdapterDataSource
    public func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return items
    }

    public func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        //override me
        return ListSectionController()
    }

    public func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return emptyView
    }

//    override func hideAllLoaders() {
//        super.hideAllLoaders()
//        self.stopRefreshing()
//    }

}

//// MARK: - UIScrollViewDelegate
//extension BaseCollectionViewController: UIScrollViewDelegate {
//    public func scrollViewWillEndDragging(_ scrollView: UIScrollView,
//                                          withVelocity velocity: CGPoint,
//                                          targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        let distance = scrollView.contentSize.height - (targetContentOffset.pointee.y + scrollView.bounds.height)
//        if canLoad && distance < 200 {
//            canLoad = true
//            self.loadMore()
//        }
//    }
//}

