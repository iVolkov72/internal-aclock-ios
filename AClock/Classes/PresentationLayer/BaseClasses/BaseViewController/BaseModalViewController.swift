//
//  BaseModalViewController.swift
//  HouseInventory
//
//  Created by Kirill Kunst on 11/09/2017.
//  Copyright © 2017 MintRocket. All rights reserved.
//

import Foundation
import UIKit

class BaseModalViewController: BaseViewController {
    @IBOutlet weak var backgroundView: UIView!

    override func setup() {
        super.setup()

//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissBackgroundTapped))
        //self.backgroundView.addGestureRecognizer(tapGesture)
    }


    func dismissBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}
