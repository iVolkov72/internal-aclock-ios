//
//  BaseViewController.swift
//  Inventory+
//
//  Created by Kirill Kunst on 02/08/16.
//  Copyright © 2016 MintRocket LLC. All rights reserved.
//

import UIKit
import RxSwift
import IQKeyboardManagerSwift

class BaseViewController: UIViewController, ErrorHandling {

    var keyboardHandler: IQKeyboardReturnKeyHandler?

    let disposeBag = DisposeBag()
    var alertController: UIAlertController?

    deinit {
        print("[D] \(self) destroyed")
        NotificationCenter.default.removeObserver(self)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBackButton()
        self.setup()
    }

    func initialize() {

    }

    func setup() {
    }

    func bindUI() {

    }

    func enableNextButtons() {
        self.keyboardHandler = IQKeyboardReturnKeyHandler(controller: self)
        self.keyboardHandler?.delegate = self
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func setupBackButton() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "" ,
                                                                style: .plain,
                                                                target:nil,
                                                                action:nil)
    }
}

extension BaseViewController: UITextViewDelegate, UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}

// MARK: - Theme

extension BaseViewController {
//    func theme() -> InventoryTheme {
//        return MainTheme.shared
//    }
}
