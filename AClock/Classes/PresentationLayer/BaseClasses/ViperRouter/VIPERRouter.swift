//
//  ViperRouter.swift
//  Inventory+
//
//  Created by Kirill Kunst on 01/08/16.
//  Copyright © 2016 MintRocket LLC. All rights reserved.
//

import UIKit

//
// MARK: - VIPER Router Interface
protocol VIPERRouterInterface {
    /**
     Present as child of parent view controller

     - parameter vc: View controller for presenting
     - parameter parent: Parent view controller for presenting from
     */
    func presentChild(_ controller: UIViewController, from parent: UIViewController)

    func presentChild(_ controller: UIViewController, from parent: UIViewController, in view: UIView)

    /**
     Present as modal from parent view controller

     - parameter vc: View controller for presenting
     - parameter parent: Parent view controller for presenting from
     */
    func presentModal(_ controller: UIViewController, from parent: UIViewController)

    /**
     Present as *window* root view controller

     - parameter vc: View controller for presenting
     - parameter window: Window for presenting from
     */
    func present(_ controller: UIViewController, using window: UIWindow)

    /**
     Present in navigation controller

     - parameter vc: View controller for presenting
     - parameter nc: Navigation controller for pushing
     */
    func present(_ controller: UIViewController, using ncontroller: UINavigationController)

    func dismiss()
    func dismiss(toRoot: Bool)
}

//
// MARK: - VIPER Router
class VIPERRouter<T>: VIPERRouterInterface where T: UIViewController {
    weak var viewController: T!

    func presentChild(_ controller: UIViewController, from parent: UIViewController) {
       self.presentChild(controller, from: parent, in: parent.view)
    }

    func presentChild(_ controller: UIViewController, from parent: UIViewController, in view: UIView) {
        let old = parent.childViewControllers.first
        old?.view.removeFromSuperview()
        old?.removeFromParentViewController()
        parent.addChildViewController(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(controller.view)
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: view.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        //view.fadeTransition()
    }

    func presentModal(_ controller: UIViewController, from parent: UIViewController) {
//        if let vc = controller as? BaseViewController {
//            vc.setupInteraction()
//            controller.transitioningDelegate = vc
//        } else if let nc = controller as? UINavigationController,
//                  let vc = nc.viewControllers.first as? BaseViewController {
//            vc.setupInteraction()
//            controller.transitioningDelegate = vc
//        }
//
//        parent.present(controller, animated: true, completion: { })
    }

    func presentModal(_ controller: UIViewController) {
//        var window: UIWindow? = MRWindow.create(level: UIWindowLevelStatusBar - 1)
//        window?.rootViewController?.present(controller, animated: true) { _ in
//            window = nil
//        }
    }

    func present(_ controller: UIViewController, using window: UIWindow) {
        if let old = window.rootViewController {
            old.dismiss(animated: false, completion: {
                old.view.removeFromSuperview()
            })
        }
        window.rootViewController = controller
        window.makeKeyAndVisible()
        UIView.transition(with: window,
                          duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }

    func present(_ controller: UIViewController, using ncontroller: UINavigationController) {
        if ncontroller.topViewController != controller {
            ncontroller.pushViewController(controller, animated: true)
        }
    }

    public func dismiss() {
        dismiss(toRoot: false)
    }

    public func dismiss(toRoot: Bool) {
        if let nc = self.viewController.navigationController {
            if nc.viewControllers.count > 1 {
                if toRoot {
                    nc.popToRootViewController(animated: true)
                } else {
                    nc.popViewController(animated: true)
                }
            } else {
                nc.dismiss(animated: true, completion: nil)
            }
        } else {
            self.viewController?.dismiss(animated: true, completion: nil)
        }
    }

    func openMain() {
        MainAppCoordinator.shared.openMainModule()
    }

}
