//
//  AlertErrorHandler.swift
//  Inventory+
//
//  Created by Kirill Kunst on 24/10/2016.
//  Copyright © 2016 Tewtor Limited. All rights reserved.
//

import Foundation
import Localize_Swift

class AlertErrorHandler: ErrorHandleType {
    func handleError(error: Error) {
        var message = "\(error)"
        let nsError = error as NSError
        if let m = nsError.userInfo[NSLocalizedDescriptionKey] as? String {
            message = m
        }
//        MRAlertView.alert(Constants.Titles.errorKey.localized(),
//                          message: message)
    }
}
