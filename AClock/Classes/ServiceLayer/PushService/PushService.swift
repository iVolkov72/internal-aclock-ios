//
//  PushService.swift
//  AClock
//
//  Created by Mint Rocket on 10.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import UIKit
import UserNotifications
import SwiftDate
import DefaultsKit

class PushService: NSObject {
    
    let key = Key<[AClockItem]>("defaultsClockKey")
    lazy var items = Defaults.shared.get(for: key)

    @discardableResult
    func createNotification(time: Date) -> UILocalNotification {
        let notification = UILocalNotification()
        let stringTime = time.string(format: .custom("dd.MM.yyyy HH:mm"))
        
        let dateFormatter = DateFormatter()
        let dateFormatter2 = DateFormatter()
        let dateFormatter3 = DateFormatter()

        dateFormatter.dateFormat = "dd.MM.yyyy"
        dateFormatter2.dateFormat = "HH:mm"
        dateFormatter3.dateFormat = "dd.MM.yyyy HH:mm"

        let updateTime: Date = dateFormatter.date(from: dateFormatter.string(from: Date()))!
        let newTime = dateFormatter.string(from: updateTime) + " " + dateFormatter2.string(from: time)
        let newFireDate = dateFormatter3.date(from: newTime)
        
        notification.alertBody = "Будильник на \(stringTime)"
        notification.alertTitle = "Будильник"
        notification.applicationIconBadgeNumber = 1
        notification.alertAction = "alarmChanged"
        notification.fireDate = newFireDate
        notification.timeZone = NSTimeZone.local
        notification.soundName = "Muzyka_na_telefon_-_iPhone_Ringtone_Pesni-Tut.mp3"

        if let count = UIApplication.shared.scheduledLocalNotifications?.count {
            notification.userInfo = ["key" : count]
        }
        notification.repeatInterval = .day
        
        UIApplication.shared.scheduleLocalNotification(notification)
        print("Notification created!")
        
        return notification
    }
    
    func changeDate(time: Date) -> Date {
        _ = time.timeIntervalSince(Date())
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        print("changeDate return = \(formatter.date(from: formatter.string(from: time))!)")
        return formatter.date(from: formatter.string(from: time))!
    }
    
    func deletePush(item: AClockItem) {
        if let result = UIApplication.shared.scheduledLocalNotifications?.first(where: { (notification) -> Bool in
            return notification.userInfo?["key"] as? Int == item.key} )
        {
            UIApplication.shared.cancelLocalNotification(result)
        }
    }
    
    func getActiveNotifications() -> [Int : UILocalNotification] {
        var pushes: [Int : UILocalNotification] = [:]
        var counter = 0
        if let notifications = UIApplication.shared.scheduledLocalNotifications {
            for notification in notifications {
                pushes.updateValue(notification, forKey: counter)
                counter += 1
            }
        }
        return pushes
    }
    
    func updateItem(item: AClockItem) {
        let defaults = Defaults.shared
        if let itemsArr = items {
            defaults.set(itemsArr, for: key)
        }
    }
    
    func createItem(notification: UILocalNotification) -> AClockItem {
        let defaults = Defaults.shared
        let item = AClockItem(notification: notification)
        
        var itemsArr = items ?? []
    
        itemsArr.append(item)
        defaults.set(itemsArr, for: key)
        
        return item
    }
    
    func restoreAllPushes() -> [AClockItem]? {
        return items
    }
}
